package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 李俣浩
 * @Date 2022/9/18 16:08
 */
@Controller
public class TestController {
    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        return "hello word test02";
    }

    @RequestMapping("/light")
    public String light(){
        return "light";
    }
}
